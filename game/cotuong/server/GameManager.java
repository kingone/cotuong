/*     */ package game.cotuong.server;
/*     */ 
/*     */ import game.cotuong.server.cmd.send.SendUpdateAutoStart;
/*     */ import game.cotuong.server.logic.Gamble;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class GameManager
/*     */ {
/*     */   public static final int GS_NO_START = 0;
/*     */   public static final int GS_GAME_PLAYING = 1;
/*     */   public static final int GS_GAME_END = 3;
/*     */   public static final int NO_ACTION = -1;
/*     */   public static final int IN_TURN = 0;
/*     */   public static final int CHANGE_TURN = 1;
/*  20 */   public int roomOwnerChair = 20;
/*     */   public int roomCreatorUserId;
/*  22 */   public int rCuocLon = 1;
/*     */   
/*     */ 
/*  25 */   public volatile int gameState = 0;
/*  26 */   public volatile int gameAction = -1;
/*     */   
/*     */ 
/*  29 */   public volatile int countDown = 0;
/*  30 */   public volatile boolean isAutoStart = false;
/*     */   
/*  32 */   public volatile int currentChair = -1;
/*  33 */   public volatile int lastWinChair = -1;
/*     */   public Gamble game;
/*     */   public CotuongGameServer gameServer;
/*     */   
/*     */   public GameManager()
/*     */   {
/*  39 */     this.game = new Gamble();
/*     */   }
/*     */   
/*  42 */   public int getGameState() { return this.gameState; }
/*     */   
/*     */   public void prepareNewGame() {
/*  45 */     this.game.reset();
/*  46 */     this.isAutoStart = false;
/*  47 */     this.gameServer.kiemTraTuDongBatDau(5);
/*     */   }
/*     */   
/*  50 */   public void gameLoop() { if ((this.gameState == 0) && (this.isAutoStart)) {
/*  51 */       this.countDown -= 1;
/*  52 */       if (this.countDown <= 0) {
/*  53 */         this.gameState = 1;
/*  54 */         this.gameServer.start();
/*     */       }
/*     */     }
/*  57 */     else if ((this.gameState == 0) && (!this.isAutoStart)) {
/*  58 */       if (this.gameServer.playerCount > 1) {
/*  59 */         this.gameServer.kiemTraTuDongBatDau(5);
/*     */       }
/*     */     }
/*  62 */     else if (this.gameState == 1) {
/*  63 */       this.countDown -= 1;
/*  64 */       updatePlayingTime();
/*  65 */       if (this.countDown <= 0) {
/*  66 */         if (this.gameAction == 1) {
/*  67 */           changeTurn();
/*     */         }
/*  69 */         else if (this.gameAction == 0) {
/*  70 */           tudongChoi();
/*     */         }
/*     */       }
/*  73 */     } else if (this.gameState == 3) {
/*  74 */       this.countDown -= 1;
/*  75 */       if (this.countDown <= 0) {
/*  76 */         this.gameServer.pPrepareNewGame();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*  81 */   public void updatePlayingTime() { this.gameServer.updatePlayingTime(); }
/*     */   
/*     */   public void notifyAutoStartToUsers(int after) {
/*  84 */     SendUpdateAutoStart msg = new SendUpdateAutoStart();
/*  85 */     msg.isAutoStart = this.isAutoStart;
/*  86 */     msg.autoStartTime = ((byte)after);
/*  87 */     this.gameServer.send(msg);
/*     */   }
/*     */   
/*  90 */   public void cancelAutoStart() { if (this.isAutoStart) {
/*  91 */       this.isAutoStart = false;
/*  92 */       notifyAutoStartToUsers(0);
/*     */     }
/*     */   }
/*     */   
/*  96 */   public void makeAutoStart(int after) { if (this.gameState != 0) return;
/*  97 */     if (!this.isAutoStart)
/*     */     {
/*  99 */       this.countDown = after;
/*     */ 
/*     */ 
/*     */     }
/* 103 */     else if (after < this.countDown) {
/* 104 */       this.countDown = after;
/*     */     } else {
/* 106 */       after = this.countDown;
/*     */     }
/*     */     
/* 109 */     this.isAutoStart = true;
/* 110 */     notifyAutoStartToUsers(after);
/*     */   }
/*     */   
/* 113 */   private void tudongChoi() { this.gameServer.tudongChoi(); }
/*     */   
/*     */   private void changeTurn() {
/* 116 */     this.gameServer.changeTurn();
/*     */   }
/*     */   
/* 119 */   public int currentChair() { return this.currentChair; }
/*     */   
/*     */   public boolean canOutRoom()
/*     */   {
/* 123 */     return getGameState() == 0;
/*     */   }
/*     */ }


/* Location:              C:\gamebai\cotuong\Cotuong.jar!\game\cotuong\server\GameManager.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */