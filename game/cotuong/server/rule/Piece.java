/*    */ package game.cotuong.server.rule;
/*    */ 
/*    */ 
/*    */ public class Piece
/*    */   implements Cloneable
/*    */ {
/*    */   public String key;
/*    */   
/*    */   public char color;
/*    */   
/*    */   public char character;
/*    */   public char index;
/* 13 */   public int[] position = new int[2];
/*    */   
/*    */   public Piece(String name, int[] position) {
/* 16 */     this.key = name;
/* 17 */     this.color = name.charAt(0);
/* 18 */     this.character = name.charAt(1);
/* 19 */     this.index = name.charAt(2);
/* 20 */     this.position = position;
/*    */   }
/*    */ }


/* Location:              C:\gamebai\cotuong\Cotuong.jar!\game\cotuong\server\rule\Piece.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */