/*     */ package game.cotuong.server.rule.ai;
/*     */ 
/*     */ import game.cotuong.server.rule.Board;
/*     */ import game.cotuong.server.rule.Piece;
/*     */ import java.util.Map;
/*     */ import java.util.Map.Entry;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class EvalModel
/*     */ {
/*  15 */   private int[][] values = new int[2][2];
/*     */   
/*     */ 
/*     */ 
/*     */   public int eval(Board board, char player)
/*     */   {
/*  21 */     for (Map.Entry<String, Piece> stringPieceEntry : board.pieces.entrySet()) {
/*  22 */       Piece piece = (Piece)stringPieceEntry.getValue();
/*     */       
/*  24 */       int[] reversePosition = { 10 - 1 - piece.position[0], piece.position[1] };
/*  25 */       switch (piece.character) {
/*     */       case 'b': 
/*  27 */         if (piece.color == 'r') this.values[0][0] += evalPieceValue(0); else
/*  28 */           this.values[1][0] += evalPieceValue(0);
/*  29 */         break;
/*     */       case 's': 
/*  31 */         if (piece.color == 'r') this.values[0][0] += evalPieceValue(1); else
/*  32 */           this.values[1][0] += evalPieceValue(1);
/*  33 */         break;
/*     */       case 'x': 
/*  35 */         if (piece.color == 'r') this.values[0][0] += evalPieceValue(2); else
/*  36 */           this.values[1][0] += evalPieceValue(2);
/*  37 */         break;
/*     */       case 'm': 
/*  39 */         if (piece.color == 'r') {
/*  40 */           this.values[0][0] += evalPieceValue(3);
/*  41 */           this.values[0][1] += evalPiecePosition(3, piece.position);
/*     */         } else {
/*  43 */           this.values[1][0] += evalPieceValue(3);
/*  44 */           this.values[1][1] += evalPiecePosition(3, reversePosition);
/*     */         }
/*  46 */         break;
/*     */       case 'j': 
/*  48 */         if (piece.color == 'r') {
/*  49 */           this.values[0][0] += evalPieceValue(4);
/*  50 */           this.values[0][1] += evalPiecePosition(4, piece.position);
/*     */         } else {
/*  52 */           this.values[1][0] += evalPieceValue(4);
/*  53 */           this.values[1][1] += evalPiecePosition(4, reversePosition);
/*     */         }
/*  55 */         break;
/*     */       case 'p': 
/*  57 */         if (piece.color == 'r') {
/*  58 */           this.values[0][0] += evalPieceValue(5);
/*  59 */           this.values[0][1] += evalPiecePosition(5, piece.position);
/*     */         } else {
/*  61 */           this.values[1][0] += evalPieceValue(5);
/*  62 */           this.values[1][1] += evalPiecePosition(5, reversePosition);
/*     */         }
/*  64 */         break;
/*     */       case 'z': 
/*  66 */         if (piece.color == 'r') {
/*  67 */           this.values[0][0] += evalPieceValue(6);
/*  68 */           this.values[0][1] += evalPiecePosition(6, piece.position);
/*     */         } else {
/*  70 */           this.values[1][0] += evalPieceValue(6);
/*  71 */           this.values[1][1] += evalPiecePosition(6, reversePosition);
/*     */         }
/*     */         break;
/*     */       }
/*     */     }
/*  76 */     int sumRed = this.values[0][0] + this.values[0][1] * 8;int sumBlack = this.values[1][0] + this.values[1][1] * 8;
/*  77 */     switch (player) {
/*     */     case 'r': 
/*  79 */       return sumRed - sumBlack;
/*     */     case 'b': 
/*  81 */       return sumBlack - sumRed;
/*     */     }
/*  83 */     return -1;
/*     */   }
/*     */   
/*     */ 
/*     */   private int evalPieceValue(int p)
/*     */   {
/*  89 */     int[] pieceValue = { 1000000, 110, 110, 300, 600, 300, 70 };
/*  90 */     return pieceValue[p];
/*     */   }
/*     */   
/*     */   private int evalPiecePosition(int p, int[] pos) {
/*  94 */     int[][] pPosition = { { 6, 4, 0, -10, -12, -10, 0, 4, 6 }, { 2, 2, 0, -4, -14, -4, 0, 2, 2 }, { 2, 2, 0, -10, -8, -10, 0, 2, 2 }, { 0, 0, -2, 4, 10, 4, -2, 0, 0 }, { 0, 0, 0, 2, 8, 2, 0, 0, 0 }, { -2, 0, 4, 2, 6, 2, 4, 0, -2 }, { 0, 0, 0, 2, 4, 2, 0, 0, 0 }, { 4, 0, 8, 6, 10, 6, 8, 0, 4 }, { 0, 2, 4, 6, 6, 6, 4, 2, 0 }, { 0, 0, 2, 6, 6, 6, 2, 0, 0 } };
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 106 */     int[][] mPosition = { { 4, 8, 16, 12, 4, 12, 16, 8, 4 }, { 4, 10, 28, 16, 8, 16, 28, 10, 4 }, { 12, 14, 16, 20, 18, 20, 16, 14, 12 }, { 8, 24, 18, 24, 20, 24, 18, 24, 8 }, { 6, 16, 14, 18, 16, 18, 14, 16, 6 }, { 4, 12, 16, 14, 12, 14, 16, 12, 4 }, { 2, 6, 8, 6, 10, 6, 8, 6, 2 }, { 4, 2, 8, 8, 4, 8, 8, 2, 4 }, { 0, 2, 4, 4, -2, 4, 4, 2, 0 }, { 0, -4, 0, 0, 0, 0, 0, -4, 0 } };
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 118 */     int[][] jPosition = { { 14, 14, 12, 18, 16, 18, 12, 14, 14 }, { 16, 20, 18, 24, 26, 24, 18, 20, 16 }, { 12, 12, 12, 18, 18, 18, 12, 12, 12 }, { 12, 18, 16, 22, 22, 22, 16, 18, 12 }, { 12, 14, 12, 18, 18, 18, 12, 14, 12 }, { 12, 16, 14, 20, 20, 20, 14, 16, 12 }, { 6, 10, 8, 14, 14, 14, 8, 10, 6 }, { 4, 8, 6, 14, 12, 14, 6, 8, 4 }, { 8, 4, 8, 16, 8, 16, 8, 4, 8 }, { -2, 10, 6, 14, 12, 14, 6, 10, -2 } };
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 130 */     int[][] zPosition = { { 0, 3, 6, 9, 12, 9, 6, 3, 0 }, { 18, 36, 56, 80, 120, 80, 56, 36, 18 }, { 14, 26, 42, 60, 80, 60, 42, 26, 14 }, { 10, 20, 30, 34, 40, 34, 30, 20, 10 }, { 6, 12, 18, 18, 20, 18, 18, 12, 6 }, { 2, 0, 8, 0, 8, 0, 8, 0, 2 }, { 0, 0, -2, 0, 4, 0, -2, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0, 0 } };
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 142 */     if (p == 3) return mPosition[pos[0]][pos[1]];
/* 143 */     if (p == 4) return jPosition[pos[0]][pos[1]];
/* 144 */     if (p == 5) return pPosition[pos[0]][pos[1]];
/* 145 */     if (p == 6) return zPosition[pos[0]][pos[1]];
/* 146 */     return -1;
/*     */   }
/*     */   
/*     */   private int evalPieceControl() {
/* 150 */     return 0;
/*     */   }
/*     */   
/*     */   private int evalPieceFlexible(int p)
/*     */   {
/* 155 */     int[] pieceFlexible = { 0, 1, 1, 13, 7, 7, 15 };
/* 156 */     return 0;
/*     */   }
/*     */   
/*     */   private int evalPieceProtect() {
/* 160 */     return 0;
/*     */   }
/*     */   
/*     */   private int evalPieceFeature() {
/* 164 */     return 0;
/*     */   }
/*     */ }


/* Location:              C:\gamebai\cotuong\Cotuong.jar!\game\cotuong\server\rule\ai\EvalModel.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */