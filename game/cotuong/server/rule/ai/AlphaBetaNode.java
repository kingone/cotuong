/*    */ package game.cotuong.server.rule.ai;
/*    */ 
/*    */ 
/*    */ public class AlphaBetaNode
/*    */ {
/*    */   public String piece;
/*    */   
/*    */   public int[] from;
/*    */   
/*    */   public int[] to;
/*    */   public int value;
/*    */   
/*    */   public AlphaBetaNode(String piece, int[] from, int[] to)
/*    */   {
/* 15 */     this.piece = piece;
/* 16 */     this.from = from;
/* 17 */     this.to = to;
/*    */   }
/*    */ }


/* Location:              C:\gamebai\cotuong\Cotuong.jar!\game\cotuong\server\rule\ai\AlphaBetaNode.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */