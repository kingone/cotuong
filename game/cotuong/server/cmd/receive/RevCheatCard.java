/*    */ package game.cotuong.server.cmd.receive;
/*    */ 
/*    */ import bitzero.server.extensions.data.BaseCmd;
/*    */ import bitzero.server.extensions.data.DataCmd;
/*    */ import java.nio.ByteBuffer;
/*    */ 
/*    */ public class RevCheatCard
/*    */   extends BaseCmd
/*    */ {
/*    */   public byte firstChair;
/* 11 */   public boolean isCheat = false;
/*    */   public byte[] cards;
/*    */   public long[] moneyArray;
/*    */   public int chair;
/*    */   
/* 16 */   public RevCheatCard(DataCmd dataCmd) { super(dataCmd);
/* 17 */     unpackData();
/*    */   }
/*    */   
/*    */   public void unpackData()
/*    */   {
/* 22 */     ByteBuffer bf = makeBuffer();
/* 23 */     this.isCheat = readBoolean(bf);
/* 24 */     this.firstChair = readByte(bf);
/* 25 */     if (this.isCheat) {
/* 26 */       this.cards = readByteArray(bf);
/* 27 */       this.moneyArray = readLongArray(bf);
/* 28 */       this.chair = bf.get();
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\gamebai\cotuong\Cotuong.jar!\game\cotuong\server\cmd\receive\RevCheatCard.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */