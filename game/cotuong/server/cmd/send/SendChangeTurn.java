/*    */ package game.cotuong.server.cmd.send;
/*    */ 
/*    */ import bitzero.server.extensions.data.BaseMsg;
/*    */ import java.nio.ByteBuffer;
/*    */ 
/*    */ 
/*    */ 
/*    */ public class SendChangeTurn
/*    */   extends BaseMsg
/*    */ {
/*    */   public int curentChair;
/*    */   public int turnTime;
/*    */   public int gameTime;
/*    */   
/*    */   public SendChangeTurn()
/*    */   {
/* 17 */     super((short)3104);
/*    */   }
/*    */   
/*    */   public byte[] createData() {
/* 21 */     ByteBuffer bf = makeBuffer();
/* 22 */     bf.put((byte)this.curentChair);
/* 23 */     bf.putInt(this.turnTime);
/* 24 */     bf.putInt(this.gameTime);
/* 25 */     return packBuffer(bf);
/*    */   }
/*    */ }


/* Location:              C:\gamebai\cotuong\Cotuong.jar!\game\cotuong\server\cmd\send\SendChangeTurn.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */