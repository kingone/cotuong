/*    */ package game.cotuong.server.cmd.send;
/*    */ 
/*    */ import bitzero.server.extensions.data.BaseMsg;
/*    */ import game.cotuong.server.GamePlayer;
/*    */ import game.cotuong.server.sPlayerInfo;
/*    */ import game.entities.PlayerInfo;
/*    */ import game.modules.gameRoom.entities.GameMoneyInfo;
/*    */ import java.nio.ByteBuffer;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class SendGameInfo
/*    */   extends BaseMsg
/*    */ {
/*    */   public int maxUserPerRoom;
/*    */   public byte chair;
/*    */   public String[][] map;
/*    */   public int gameState;
/*    */   public int gameAction;
/*    */   public int currentChair;
/*    */   public int countdownTime;
/*    */   public int moneyType;
/*    */   public long roomBet;
/*    */   public int gameId;
/*    */   public int roomId;
/* 34 */   public boolean[] hasInfoAtChair = new boolean[20];
/* 35 */   public GamePlayer[] pInfos = new GamePlayer[20];
/* 36 */   public byte[] lastMove = new byte[2];
/*    */   
/*    */   public SendGameInfo() {
/* 39 */     super((short)3110);
/*    */   }
/*    */   
/*    */   public byte[] createData() {
/* 43 */     ByteBuffer bf = makeBuffer();
/*    */     
/* 45 */     bf.put((byte)this.maxUserPerRoom);
/* 46 */     bf.put(this.chair);
/* 47 */     bf.putShort((short)this.map.length);
/* 48 */     for (int i = 0; i < this.map.length; i++) {
/* 49 */       putStringArray(bf, this.map[i]);
/*    */     }
/* 51 */     bf.put((byte)this.gameState);
/* 52 */     bf.put((byte)this.gameAction);
/* 53 */     bf.put((byte)this.countdownTime);
/* 54 */     bf.put((byte)this.currentChair);
/* 55 */     bf.put((byte)this.moneyType);
/* 56 */     putLong(bf, this.roomBet);
/* 57 */     bf.putInt(this.gameId);
/* 58 */     bf.putInt(this.roomId);
/*    */     
/* 60 */     putBooleanArray(bf, this.hasInfoAtChair);
/* 61 */     for (int i = 0; i < 20; i++) {
/* 62 */       if (this.hasInfoAtChair[i]) {
/* 63 */         GamePlayer gp = this.pInfos[i];
/*    */         
/* 65 */         bf.put((byte)gp.playerStatus);
/* 66 */         PlayerInfo pInfo = gp.pInfo;
/* 67 */         putStr(bf, pInfo.avatarUrl);
/* 68 */         putStr(bf, pInfo.nickName);
/* 69 */         if (gp.gameMoneyInfo != null) {
/* 70 */           putLong(bf, gp.gameMoneyInfo.currentMoney);
/*    */         } else {
/* 72 */           putLong(bf, 0L);
/*    */         }
/* 74 */         bf.put((byte)gp.gameChair);
/* 75 */         bf.put((byte)gp.spInfo.chessColor);
/* 76 */         bf.putInt(gp.spInfo.gameTime);
/*    */       }
/*    */     }
/* 79 */     putByteArray(bf, this.lastMove);
/* 80 */     return packBuffer(bf);
/*    */   }
/*    */   
/*    */   public void initPrivateInfo(GamePlayer gp) {
/* 84 */     this.chair = ((byte)gp.chair);
/*    */   }
/*    */ }


/* Location:              C:\gamebai\cotuong\Cotuong.jar!\game\cotuong\server\cmd\send\SendGameInfo.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */