/*    */ package game.cotuong.server.cmd.send;
/*    */ 
/*    */ import bitzero.server.extensions.data.BaseMsg;
/*    */ import game.cotuong.server.GamePlayer;
/*    */ import game.entities.PlayerInfo;
/*    */ import game.modules.gameRoom.entities.GameMoneyInfo;
/*    */ import java.nio.ByteBuffer;
/*    */ 
/*    */ public class SendDangKyChoi extends BaseMsg
/*    */ {
/*    */   public static final byte DANG_KY = 1;
/*    */   public static final byte HUY = 2;
/*    */   public static final byte DANG_KY_HUY = 3;
/* 14 */   public byte action = 1;
/* 15 */   public GamePlayer gp = null;
/*    */   
/* 17 */   public SendDangKyChoi() { super((short)3108); }
/*    */   
/*    */   public byte[] createData()
/*    */   {
/* 21 */     ByteBuffer bf = makeBuffer();
/* 22 */     if (this.gp != null) {
/* 23 */       bf.put(this.action);
/* 24 */       bf.put((byte)this.gp.gameChair);
/* 25 */       putStr(bf, this.gp.pInfo.nickName);
/* 26 */       putLong(bf, this.gp.gameMoneyInfo.getCurrentMoneyFromCache());
/*    */     } else {
/* 28 */       bf.put((byte)0);
/* 29 */       bf.put((byte)0);
/* 30 */       putStr(bf, "");
/* 31 */       putLong(bf, 0L);
/*    */     }
/* 33 */     return packBuffer(bf);
/*    */   }
/*    */ }


/* Location:              C:\gamebai\cotuong\Cotuong.jar!\game\cotuong\server\cmd\send\SendDangKyChoi.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */