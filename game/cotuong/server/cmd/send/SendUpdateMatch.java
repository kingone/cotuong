/*    */ package game.cotuong.server.cmd.send;
/*    */ 
/*    */ import bitzero.server.extensions.data.BaseMsg;
/*    */ import game.cotuong.server.GamePlayer;
/*    */ import java.nio.ByteBuffer;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class SendUpdateMatch
/*    */   extends BaseMsg
/*    */ {
/*    */   public byte chair;
/* 15 */   public boolean[] hasInfoAtChair = new boolean[20];
/* 16 */   public GamePlayer[] pInfos = new GamePlayer[20];
/*    */   
/* 18 */   public SendUpdateMatch() { super((short)3123); }
/*    */   
/*    */   public byte[] createData()
/*    */   {
/* 22 */     ByteBuffer bf = makeBuffer();
/* 23 */     bf.put(this.chair);
/* 24 */     putBooleanArray(bf, this.hasInfoAtChair);
/* 25 */     for (int i = 0; i < 20; i++) {
/* 26 */       if (this.hasInfoAtChair[i]) {
/* 27 */         GamePlayer gp = this.pInfos[i];
/* 28 */         bf.putInt(gp.playerStatus);
/*    */       }
/*    */     }
/* 31 */     return packBuffer(bf);
/*    */   }
/*    */ }


/* Location:              C:\gamebai\cotuong\Cotuong.jar!\game\cotuong\server\cmd\send\SendUpdateMatch.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */