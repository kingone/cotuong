/*    */ package game.cotuong.server.cmd.send;
/*    */ 
/*    */ import bitzero.server.extensions.data.BaseMsg;
/*    */ import java.nio.ByteBuffer;
/*    */ 
/*    */ 
/*    */ public class SendKickRoom
/*    */   extends BaseMsg
/*    */ {
/*    */   public static final byte ERROR_MONEY = 1;
/*    */   public static final byte SYSTEM_MAINTAIN = 2;
/*    */   public static final byte BUY_IN_FAIL = 3;
/*    */   public byte reason;
/*    */   
/*    */   public SendKickRoom()
/*    */   {
/* 17 */     super((short)3120);
/*    */   }
/*    */   
/*    */   public byte[] createData() {
/* 21 */     ByteBuffer bf = makeBuffer();
/* 22 */     bf.put(this.reason);
/* 23 */     return packBuffer(bf);
/*    */   }
/*    */ }


/* Location:              C:\gamebai\cotuong\Cotuong.jar!\game\cotuong\server\cmd\send\SendKickRoom.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */