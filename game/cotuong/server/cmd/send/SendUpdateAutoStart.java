/*    */ package game.cotuong.server.cmd.send;
/*    */ 
/*    */ import bitzero.server.extensions.data.BaseMsg;
/*    */ import java.nio.ByteBuffer;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class SendUpdateAutoStart
/*    */   extends BaseMsg
/*    */ {
/*    */   public boolean isAutoStart;
/*    */   public byte autoStartTime;
/*    */   
/*    */   public SendUpdateAutoStart()
/*    */   {
/* 18 */     super((short)3107);
/*    */   }
/*    */   
/*    */   public byte[] createData()
/*    */   {
/* 23 */     ByteBuffer bf = makeBuffer();
/* 24 */     putBoolean(bf, Boolean.valueOf(this.isAutoStart));
/* 25 */     bf.put(this.autoStartTime);
/* 26 */     return packBuffer(bf);
/*    */   }
/*    */ }


/* Location:              C:\gamebai\cotuong\Cotuong.jar!\game\cotuong\server\cmd\send\SendUpdateAutoStart.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */