/*    */ package game.cotuong.server.cmd.send;
/*    */ 
/*    */ import bitzero.server.extensions.data.BaseMsg;
/*    */ import java.nio.ByteBuffer;
/*    */ 
/*    */ 
/*    */ public class SendUserExitRoom
/*    */   extends BaseMsg
/*    */ {
/*    */   public byte nChair;
/*    */   public String nickName;
/*    */   
/*    */   public SendUserExitRoom()
/*    */   {
/* 15 */     super((short)3119);
/*    */   }
/*    */   
/*    */   public byte[] createData() {
/* 19 */     ByteBuffer bf = makeBuffer();
/* 20 */     bf.put(this.nChair);
/* 21 */     putStr(bf, this.nickName);
/* 22 */     return packBuffer(bf);
/*    */   }
/*    */ }


/* Location:              C:\gamebai\cotuong\Cotuong.jar!\game\cotuong\server\cmd\send\SendUserExitRoom.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */