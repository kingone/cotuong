/*    */ package game.cotuong.server.cmd.send;
/*    */ 
/*    */ import bitzero.server.extensions.data.BaseMsg;
/*    */ import java.nio.ByteBuffer;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class SendTakeTurn
/*    */   extends BaseMsg
/*    */ {
/* 13 */   public int chair = 20;
/*    */   public byte[] from;
/*    */   public byte[] to;
/*    */   public String key;
/* 17 */   public String die = "none";
/*    */   public int result;
/*    */   
/* 20 */   public SendTakeTurn() { super((short)3101); }
/*    */   
/*    */   public byte[] createData()
/*    */   {
/* 24 */     ByteBuffer bf = makeBuffer();
/* 25 */     bf.put((byte)this.chair);
/* 26 */     putByteArray(bf, this.from);
/* 27 */     putByteArray(bf, this.to);
/* 28 */     putStr(bf, this.key);
/* 29 */     putStr(bf, this.die);
/* 30 */     bf.put((byte)this.result);
/* 31 */     return packBuffer(bf);
/*    */   }
/*    */ }


/* Location:              C:\gamebai\cotuong\Cotuong.jar!\game\cotuong\server\cmd\send\SendTakeTurn.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */