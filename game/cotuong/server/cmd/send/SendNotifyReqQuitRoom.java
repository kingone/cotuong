/*    */ package game.cotuong.server.cmd.send;
/*    */ 
/*    */ import bitzero.server.extensions.data.BaseMsg;
/*    */ import java.nio.ByteBuffer;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class SendNotifyReqQuitRoom
/*    */   extends BaseMsg
/*    */ {
/*    */   public byte chair;
/*    */   public boolean reqQuitRoom;
/*    */   
/*    */   public SendNotifyReqQuitRoom()
/*    */   {
/* 19 */     super((short)3111);
/*    */   }
/*    */   
/*    */   public byte[] createData() {
/* 23 */     ByteBuffer bf = makeBuffer();
/* 24 */     bf.put(this.chair);
/* 25 */     putBoolean(bf, Boolean.valueOf(this.reqQuitRoom));
/* 26 */     return packBuffer(bf);
/*    */   }
/*    */ }


/* Location:              C:\gamebai\cotuong\Cotuong.jar!\game\cotuong\server\cmd\send\SendNotifyReqQuitRoom.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */