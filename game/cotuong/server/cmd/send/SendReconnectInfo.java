/*    */ package game.cotuong.server.cmd.send;
/*    */ 
/*    */ import bitzero.server.extensions.data.BaseMsg;
/*    */ import game.cotuong.server.GamePlayer;
/*    */ import game.cotuong.server.sPlayerInfo;
/*    */ import game.entities.PlayerInfo;
/*    */ import game.modules.gameRoom.entities.GameMoneyInfo;
/*    */ import java.nio.ByteBuffer;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class SendReconnectInfo
/*    */   extends BaseMsg
/*    */ {
/*    */   public int maxUserPerRoom;
/*    */   public byte chair;
/*    */   public String[][] map;
/*    */   public int gameState;
/*    */   public int gameAction;
/*    */   public int currentChair;
/*    */   public int countdownTime;
/*    */   public int moneyType;
/*    */   public long roomBet;
/*    */   public int gameId;
/*    */   public int roomId;
/* 30 */   public byte[] lastMove = new byte[2];
/*    */   
/*    */ 
/* 33 */   public boolean[] hasInfoAtChair = new boolean[20];
/*    */   
/* 35 */   public GamePlayer[] pInfos = new GamePlayer[20];
/*    */   
/*    */   public SendReconnectInfo() {
/* 38 */     super((short)3124);
/*    */   }
/*    */   
/*    */   public byte[] createData() {
/* 42 */     ByteBuffer bf = makeBuffer();
/*    */     
/* 44 */     bf.put((byte)this.maxUserPerRoom);
/* 45 */     bf.put(this.chair);
/* 46 */     bf.putShort((short)this.map.length);
/* 47 */     for (int i = 0; i < this.map.length; i++) {
/* 48 */       putStringArray(bf, this.map[i]);
/*    */     }
/* 50 */     bf.put((byte)this.gameState);
/* 51 */     bf.put((byte)this.gameAction);
/* 52 */     bf.put((byte)this.countdownTime);
/* 53 */     bf.put((byte)this.currentChair);
/* 54 */     bf.put((byte)this.moneyType);
/* 55 */     putLong(bf, this.roomBet);
/* 56 */     bf.putInt(this.gameId);
/* 57 */     bf.putInt(this.roomId);
/*    */     
/* 59 */     putBooleanArray(bf, this.hasInfoAtChair);
/* 60 */     for (int i = 0; i < 20; i++) {
/* 61 */       if (this.hasInfoAtChair[i]) {
/* 62 */         GamePlayer gp = this.pInfos[i];
/*    */         
/* 64 */         bf.put((byte)gp.playerStatus);
/* 65 */         PlayerInfo pInfo = gp.pInfo;
/* 66 */         putStr(bf, pInfo.avatarUrl);
/* 67 */         putStr(bf, pInfo.nickName);
/* 68 */         if (gp.gameMoneyInfo != null) {
/* 69 */           putLong(bf, gp.gameMoneyInfo.currentMoney);
/*    */         } else {
/* 71 */           putLong(bf, 0L);
/*    */         }
/* 73 */         bf.put((byte)gp.gameChair);
/* 74 */         bf.put((byte)gp.spInfo.chessColor);
/* 75 */         bf.putInt(gp.spInfo.gameTime);
/*    */       }
/*    */     }
/* 78 */     putByteArray(bf, this.lastMove);
/* 79 */     return packBuffer(bf);
/*    */   }
/*    */   
/*    */   public void initPrivateInfo(GamePlayer gp) {
/* 83 */     this.chair = ((byte)gp.chair);
/*    */   }
/*    */ }


/* Location:              C:\gamebai\cotuong\Cotuong.jar!\game\cotuong\server\cmd\send\SendReconnectInfo.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */