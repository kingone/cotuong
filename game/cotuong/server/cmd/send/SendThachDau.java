/*    */ package game.cotuong.server.cmd.send;
/*    */ 
/*    */ import bitzero.server.extensions.data.BaseMsg;
/*    */ import java.nio.ByteBuffer;
/*    */ 
/*    */ public class SendThachDau
/*    */   extends BaseMsg
/*    */ {
/*    */   public String name;
/*    */   public int moneyBet;
/*    */   
/*    */   public SendThachDau()
/*    */   {
/* 14 */     super((short)3106);
/*    */   }
/*    */   
/*    */   public byte[] createData()
/*    */   {
/* 19 */     ByteBuffer bf = makeBuffer();
/* 20 */     putStr(bf, this.name);
/* 21 */     bf.putInt(this.moneyBet);
/* 22 */     return packBuffer(bf);
/*    */   }
/*    */ }


/* Location:              C:\gamebai\cotuong\Cotuong.jar!\game\cotuong\server\cmd\send\SendThachDau.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */