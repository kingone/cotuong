/*    */ package game.cotuong.server.cmd.send;
/*    */ 
/*    */ import bitzero.server.extensions.data.BaseMsg;
/*    */ import game.entities.PlayerInfo;
/*    */ import java.nio.ByteBuffer;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class SendNewUserJoin
/*    */   extends BaseMsg
/*    */ {
/*    */   public long money;
/*    */   public String uName;
/*    */   public String avtUrl;
/*    */   public int uChair;
/*    */   public int uStatus;
/*    */   public int gameChair;
/*    */   
/*    */   public SendNewUserJoin()
/*    */   {
/* 22 */     super((short)3121);
/*    */   }
/*    */   
/*    */   public void setBaseInfo(PlayerInfo pInfo) {
/* 26 */     this.uName = pInfo.nickName;
/* 27 */     this.avtUrl = pInfo.avatarUrl;
/*    */   }
/*    */   
/*    */   public byte[] createData()
/*    */   {
/* 32 */     ByteBuffer bf = makeBuffer();
/* 33 */     putStr(bf, this.uName);
/* 34 */     putStr(bf, this.avtUrl);
/* 35 */     putLong(bf, this.money);
/* 36 */     bf.put((byte)this.gameChair);
/* 37 */     bf.put((byte)this.uChair);
/* 38 */     bf.put((byte)this.uStatus);
/* 39 */     return packBuffer(bf);
/*    */   }
/*    */ }


/* Location:              C:\gamebai\cotuong\Cotuong.jar!\game\cotuong\server\cmd\send\SendNewUserJoin.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */